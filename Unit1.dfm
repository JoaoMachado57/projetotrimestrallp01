﻿object PForm: TPForm
  Left = 0
  Top = 0
  Caption = 'PA'#205'SES'
  ClientHeight = 500
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Nome: TLabel
    Left = 24
    Top = 11
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object População: TLabel
    Left = 24
    Top = 58
    Width = 49
    Height = 13
    Caption = 'Popula'#231#227'o'
  end
  object Língua: TLabel
    Left = 24
    Top = 104
    Width = 31
    Height = 13
    Caption = 'L'#237'ngua'
  end
  object PIB: TLabel
    Left = 24
    Top = 158
    Width = 16
    Height = 13
    Caption = 'PIB'
  end
  object Editnome: TEdit
    Left = 24
    Top = 30
    Width = 260
    Height = 21
    TabOrder = 0
  end
  object editpop: TEdit
    Left = 24
    Top = 77
    Width = 259
    Height = 21
    TabOrder = 1
  end
  object editlingua: TEdit
    Left = 24
    Top = 123
    Width = 97
    Height = 21
    TabOrder = 2
  end
  object editpib: TEdit
    Left = 23
    Top = 177
    Width = 98
    Height = 24
    TabOrder = 3
  end
  object btinserir: TButton
    Left = 23
    Top = 232
    Width = 75
    Height = 41
    Caption = 'INSERIR'
    TabOrder = 4
    OnClick = btinserirClick
  end
  object ListPais: TListBox
    Left = 432
    Top = 8
    Width = 284
    Height = 433
    ItemHeight = 13
    TabOrder = 5
  end
  object btremover: TButton
    Left = 104
    Top = 232
    Width = 75
    Height = 41
    Caption = 'REMOVER'
    TabOrder = 6
    OnClick = btremoverClick
  end
  object btatualizar: TButton
    Left = 185
    Top = 232
    Width = 75
    Height = 41
    Caption = 'ATUALIZAR'
    TabOrder = 7
    OnClick = btatualizarClick
  end
  object btsalvar: TButton
    Left = 266
    Top = 232
    Width = 75
    Height = 41
    Caption = 'SALVAR'
    TabOrder = 8
    OnClick = btsalvarClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 481
    Width = 724
    Height = 19
    Panels = <>
  end
  object editsalvar: TEdit
    Left = 266
    Top = 279
    Width = 118
    Height = 21
    TabOrder = 10
  end
end
