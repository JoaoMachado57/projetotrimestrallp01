unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TPForm = class(TForm)
    Nome: TLabel;
    Editnome: TEdit;
    Popula��o: TLabel;
    editpop: TEdit;
    L�ngua: TLabel;
    PIB: TLabel;
    editlingua: TEdit;
    editpib: TEdit;
    btinserir: TButton;
    ListPais: TListBox;
    btremover: TButton;
    btatualizar: TButton;
    btsalvar: TButton;
    StatusBar1: TStatusBar;
    editsalvar: TEdit;
    procedure btinserirClick(Sender: TObject);
    procedure btremoverClick(Sender: TObject);
    procedure btatualizarClick(Sender: TObject);
    procedure btsalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TEstudante = class(TObject)
    nome, pop, lingua, pib : string;


  end;


var
  PForm: TPForm;
  estudante : TEstudante;
  arquivosalvo : textfile;

implementation

{$R *.dfm}

procedure TPForm.btinserirClick(Sender: TObject);

begin
  estudante := TEstudante.Create;
  estudante.nome := editnome.Text;
  estudante.pop := Editpop.Text;
  estudante.lingua := Editlingua.Text;
  estudante.pib := Editpib.Text;

   ListPais.Items.AddObject(estudante.nome, estudante);
end;

procedure TPForm.btremoverClick(Sender: TObject);
begin
  ListPais.DeleteSelected;
end;

procedure TPForm.btatualizarClick(Sender: TObject);
begin
   if (ListPais.ItemIndex = 0) and (editnome.Text <> '') Then
    begin
       estudante.nome := Editnome.Text;
       PForm.ListPais.Items [ListPais.ItemIndex] := estudante.nome;
    end

    else if (ListPais.ItemIndex = 1) and (editpop.Text <> '') Then
    begin
       estudante.pop := Editpop.Text;
       PForm.ListPais.Items [ListPais.ItemIndex] := estudante.pop;
    end

    else if (listpais.ItemIndex = 2) and (editlingua.Text <> '') Then
    begin
       estudante.lingua := Editlingua.Text;
       PForm.ListPais.Items [ListPais.ItemIndex] := estudante.lingua;
    end

    else if (ListPais.ItemIndex = 3) and (editpib.Text <> '') Then
    begin
       estudante.pib := Editpib.Text;
       PForm.ListPais.Items [ListPais.ItemIndex] := estudante.pib;
    end

    else
       showmessage ('Selecione um item');

end;

procedure TPForm.btsalvarClick(Sender: TObject);
begin
if (editsalvar.Text <> '') then
    begin
    AssignFile(arquivosalvo, Editsalvar.Text+' .txt');
    Rewrite (arquivosalvo);
    WriteLn (arquivosalvo, 'Nome: '+estudante.nome);
    WriteLn (arquivosalvo, 'Popula��o: '+estudante.pop);
    WriteLn (arquivosalvo, 'L�ngua: '+estudante.lingua);
    WriteLn (arquivosalvo, '�PIB: '+estudante.pib);
    CloseFile (arquivosalvo);
    ShowMessage ('Arquivo Salvo');
    end
    else
    begin
        showmessage ('CAMPO VAZIO');
    end;

end;

end.
